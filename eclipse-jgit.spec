%global gittag 5.13.0.202109080827-r
Name:                eclipse-jgit
Version:             5.13.0
Release:             1
Summary:             Eclipse JGit
License:             BSD
URL:                 https://www.eclipse.org/jgit/
Source0:             https://git.eclipse.org/c/jgit/jgit.git/snapshot/jgit-%{gittag}.tar.gz
Patch0:              0001-Ensure-the-correct-classpath-is-set-for-the-jgit-com.patch
Patch1:              pom_for_bcpkix.patch
BuildArch:           noarch
ExcludeArch:         s390 %{arm} %{ix86}
BuildRequires:       maven-local mvn(args4j:args4j) mvn(com.google.code.gson:gson)
BuildRequires:       mvn(com.googlecode.javaewah:JavaEWAH) mvn(com.jcraft:jsch)
BuildRequires:       mvn(com.jcraft:jzlib) mvn(javax.servlet:javax.servlet-api) <= 3.1.0 mvn(junit:junit)
BuildRequires:       mvn(net.i2p.crypto:eddsa) mvn(org.apache.ant:ant)
BuildRequires:       mvn(org.apache.commons:commons-compress)
BuildRequires:       mvn(org.apache.httpcomponents:httpclient)
BuildRequires:       mvn(org.apache.httpcomponents:httpcore)
BuildRequires:       mvn(org.apache.maven.plugins:maven-antrun-plugin)
BuildRequires:       mvn(org.apache.sshd:sshd-osgi) >= 2.6.0 mvn(org.apache.sshd:sshd-sftp) >= 2.6.0
BuildRequires:       mvn(org.bouncycastle:bcpg-jdk15on) >= 1.65
BuildRequires:       mvn(org.bouncycastle:bcpkix-jdk15on) >= 1.65
BuildRequires:       mvn(org.bouncycastle:bcprov-jdk15on) >= 1.65
BuildRequires:       mvn(org.codehaus.mojo:build-helper-maven-plugin)
BuildRequires:       mvn(org.eclipse.jetty:jetty-servlet) mvn(org.osgi:osgi.core)
BuildRequires:       mvn(org.slf4j:slf4j-api) mvn(org.slf4j:slf4j-simple) mvn(org.tukaani:xz)
BuildRequires:       tycho jgit = %{version}
%if %{with tests}
BuildRequires:       git mvn(org.hamcrest:hamcrest-library) mvn(org.mockito:mockito-core)
%endif
Requires:            bouncycastle >= 1.65 apache-sshd >= 1:2.6.0
Requires:            eclipse-platform jgit = %{version}
%description
A pure Java implementation of the Git version control system.
%package javadoc
Summary:             API documentation for %{name}
%description javadoc
%{summary}.

%prep
%setup -n jgit-%{gittag} -q
%patch0 -p1
%patch1 -p1
rm .mvn/maven.config
%pom_xpath_remove "pom:configuration/pom:annotationProcessorPaths"
%pom_xpath_remove "pom:configuration/pom:compilerArgs"
%pom_change_dep -r org.osgi:org.osgi.core org.osgi:osgi.core:provided
%pom_disable_module org.eclipse.jgit.coverage
%pom_disable_module org.eclipse.jgit.benchmarks
%pom_remove_plugin :jacoco-maven-plugin
%pom_remove_plugin :maven-javadoc-plugin
%pom_remove_plugin -r :maven-source-plugin
%pom_remove_plugin :maven-enforcer-plugin
%pom_remove_plugin -r :japicmp-maven-plugin
sed -i -e 's/@{argLine}//' $(find -name pom.xml)
%pom_remove_plugin org.codehaus.mojo:build-helper-maven-plugin org.eclipse.jgit.pgm
%pom_remove_plugin :spring-boot-maven-plugin org.eclipse.jgit.pgm
%pom_xpath_remove "pom:plugins/pom:plugin/pom:executions/pom:execution[pom:id='create_jgit']" org.eclipse.jgit.pgm
sed -i -e 's/org\.springframework\.boot\.loader\.JarLauncher/org.eclipse.jgit.pgm.Main/' \
  org.eclipse.jgit.pgm/jgit.sh
sed -i -e '/javax\.servlet/s/4\.0\.0/5.0.0/' org.eclipse.jgit.lfs.server/META-INF/MANIFEST.MF org.eclipse.jgit.pgm/META-INF/MANIFEST.MF
sed -i -e '/javax\.servlet/s/3\.2\.0/5.0.0/' org.eclipse.jgit.junit.http/META-INF/MANIFEST.MF org.eclipse.jgit.http.server/META-INF/MANIFEST.MF
%pom_remove_dep log4j:log4j . org.eclipse.jgit.pgm
%pom_change_dep org.slf4j:slf4j-log4j12 org.slf4j:slf4j-simple . org.eclipse.jgit.pgm
%if %{without tests}
sed -i -e '/\.test<\/module>/d' pom.xml
%endif
%mvn_package ":*.test" __noinstall

%pom_disable_module org.eclipse.jgit.junit
%pom_disable_module org.eclipse.jgit.junit.ssh
%pom_disable_module org.eclipse.jgit.junit.http

%build
%if %{without tests}
%mvn_build -f -- -Pjavac
%else
%mvn_build -- -Pjavac  -Dmaven.test.failure.ignore=true
%endif

%install
%mvn_install
install -dm 755 %{buildroot}%{_bindir}
install -m 755 org.eclipse.jgit.pgm/jgit.sh %{buildroot}%{_bindir}/jgit
install -dm 755 %{buildroot}%{_sysconfdir}/ant.d
cat > %{buildroot}%{_sysconfdir}/ant.d/jgit <<EOF
jgit/org.eclipse.jgit jgit/org.eclipse.jgit.ant slf4j/slf4j-api slf4j/slf4j-simple jzlib jsch commons-compress xz-java javaewah httpcomponents/httpcore httpcomponents/httpclient commons-logging commons-codec eddsa apache-sshd/sshd-osgi apache-sshd/sshd-sftp
EOF

%files -f .mfiles
%license LICENSE
%doc README.md
%{_bindir}/jgit
%config(noreplace) %{_sysconfdir}/ant.d/jgit

%files javadoc -f .mfiles-javadoc

%changelog
* Tue June 13 2023 zhuwenshuo <1003254035@qq.com> - 5.13.0-1
- Update to 5.13.0

* Sat Nov 26 2022 yaoxin <yaoxin30@h-partners.com> - 5.11.0-1
- Update to 5.11.0

* Mon Aug 17 2020 yanan li <liyanan032@huawei.com> - 5.4.0-1
- Package init
